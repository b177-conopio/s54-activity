let collection = [];

// Write the queue functions below.

function print(){

	var str = [];
	    for(var i = 0; i < collection.length; i++)
	        str += collection[i] +" ";
	    return str;
}

function enqueue(element){
	collection[collection.length-1] = element;
    collection.length++;
}

function dequeue(){
	
}

function front(){
	
}

function size(){

	return collection.length;
	
}

function isEmpty(){

	return collection.length === 0;
	
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};